/*
 * mts.c (Modem Trace Server):- Server application that routes
 *                              data coming from a tty location to
 *                              another location like a socket.
 *
 * ---------------------------------------------------------------------------
 * Copyright (c) 2011, Intel Corporation
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the Intel Corporation nor the
 * names of its contributors may be used to endorse or promote products
 * derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES,
 * (INCLUDING BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * ---------------------------------------------------------------------------
 */

/*
 *This program is unit tested only for following use cases in TEST mode:
 *
 *  1) File System (-f) option
 *      - Used a bin file in place of ttyGSM18
 *      - program was able to read and generate same size log file
 *  2) RNDIS / Socket (-p) optind
 *      - Used a bin file in place of ttyGSM18
 *      - Program waits for usb0 interface to become available
 *      - Open socket
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <getopt.h>
#include <sys/socket.h>
#include <net/if.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/ioctl.h>
#include <sys/poll.h>
#include <sys/stat.h>
#include <linux/tty.h>
#include <fcntl.h>
#include <termios.h>
#include <unistd.h>
#include <ctype.h>
#include <errno.h>
#include <signal.h>
#include <sys/types.h>
#include <cutils/sockets.h>
#include <cutils/log.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <cutils/properties.h>
#include <string.h>
#include <stdbool.h>
#include <sys/inotify.h>
#include <time.h>
#include "logtool.h"
#include "mmgr_cli.h"

/* Usage text of mts displayed with -h toggle */
#define USAGE_TXT "Usage:\n" \
    "mts -i <input> -t <out_type> -o <output>" \
    "[-r <kbytes>] [-n <count>]]\n" \
    "\n" \
    "\t-i <input>      Input file, usually a tty (/dev/gsmtty18)\n" \
    "\t-t <out_type>   Output type, f for file, p for socket and k for PTI\n" \
    "\t-o <output>     Output target.\n" \
    "\t\t\t type p: port number\n" \
    "\t\t\t type k: tty supporting kernel routing\n" \
    "\t\t\t type f: file name\n" \
    "\t-r <kbytes>     Rotate log every kbytes (10MB if " \
    "unspecified). Requires type f\n" \
    "\t-n <count>      Sets max number of rotated logs to " \
    "<count>, default 4\n"

/* Log Size in KBytes : 10Megs */
#define DEFAULT_LOG_SIZE 10*(1<<10)
/* Number of Allowed file rotations will create bplog and bplog.{1-N} where N is DEFAULT_MAX_ROTATED_NR */
#define DEFAULT_MAX_ROTATED_NR 4
/* Check Interval used in parameters of sleep in various loops */
#define CHECK_INTERVAL 2
/* aplog TAG */
#undef LOG_TAG
#define LOG_TAG "ASUS_MTS"
/* Interface name used when listening on a socket */
#define IFNAME "usb0"
/* Buffer size of receive function in logs */
#define MAXDATA 8192
/* Signal used to un-configure the ldiscs */
#define MY_SIG_UNCONF_PTI SIGUSR1
/* Inotify node to watch */
#define FILE_NAME_SZ 256
#define INOT_USB_DEV_NODE "/dev/bus/usb/001"
#define INOT_EVENT_QUEUE 512
#define INOT_EVENT_SZ sizeof(struct inotify_event)
#define INOT_BUF_SZ INOT_EVENT_QUEUE * (INOT_EVENT_SZ + FILE_NAME_SZ)
/* USB log port */
#define USB_LOG_DEV "/dev/ttyACM1"
/* Poll FDs */
#define POLL_FD_NBR 3
#define POLL_FD_LOG 0
#define POLL_FD_COM 1
#define POLL_FD_NOT 2

#define MDM_UP "MDM_UP\n"
#define MDM_DW "MDM_DW\n"
#define MDM_MSG_SZ 7

#define ATTEMPTI(fun, predicate, msg, ret) do { \
    if ((*ret = fun)predicate) { \
        LOGI("%s:%d " msg " FAILED when calling %s : returned %d!", \
        __FUNCTION__, __LINE__, #fun, *ret); \
    }}while(0)

#define ATTEMPTE(fun, predicate, msg, ret, label) do { \
    if ((*ret = fun)predicate) { \
        LOGE("%s:%d " msg " FAILED when calling %s : returned %d! because=%s", \
        __FUNCTION__, __LINE__, #fun, *ret,strerror(errno)); \
        goto label; \
    }}while(0)

/* Length of the full PATH in a file expressed in bytes */
#define PATH_LEN 256

/* FALLOC_FL_KEEP_SIZE is not exported by bionic yet, so defin'ing it here until bionic exports it */
#define FALLOC_FL_KEEP_SIZE 0x01
#define PROP_HEAD "persist.service.mts."

#define MAX_WAIT_MMGR_CONNECT_SECONDS  5
#define MMGR_CONNECT_RETRY_TIME_MS     200

#define INIT_FAIL 1
#define COND_FAIL 2
#define MUTEX_FAIL 3
#define SEARIL_PORT_NULL 4

void changeNewDatePath(char * p_output);
void sendErrorMessage(char* msg,int mode);
typedef struct args_s
{
    char *name;
    char *key;
    char *type_conv;
    void *storage;
} args;

typedef struct comm_mdm_s
{
    pthread_cond_t modem_online;
    pthread_mutex_t cond_mtx;
    int intercom[2];            /* 0 read - 1 write */
    bool modem_available;
    bool usb_logging;
    int ttyfd;
    int inotfd;
    int inotwd;
    char *p_input;
    mmgr_cli_handle_t *mmgr_hdl;
} comm_mdm;

static int
open_gsmtty (char *p_portname)
{
    struct termios t_termios;
    int tty_fd;
    int ret;
    LOGI ("%s:%d Opening %s", __FUNCTION__, __LINE__, p_portname);
    ATTEMPTE (open (p_portname, O_RDONLY | O_NONBLOCK), <0, "Opening tty",
              &tty_fd, out);
    /*
     * Setting up for tty raw mode, which is based
     * of what ttyIFX0 diagnostic apps developed.
     * I don't think we should quit in the event of an
     * error, but we should inform.
     */
    ATTEMPTI (tcgetattr (tty_fd, &t_termios), !=0, "getting attr's", &ret);
    if (ret == 0)
    {
        cfmakeraw (&t_termios);
        ATTEMPTI (tcsetattr (tty_fd, TCSANOW, &t_termios), !=0,
                  "setting attr's", &ret);
    }
    LOGI ("%s:%d %s opened", __FUNCTION__, __LINE__, p_portname);
out:
    return tty_fd;
}

static int
iface_up (char *ip_addr_if)
{
    int fd;
    int if_status = 0;
    struct ifreq ifr;

    fd = socket (AF_INET, SOCK_DGRAM, 0);
    int flags = fcntl (fd, F_GETFL, 0);
    if (fcntl (fd, F_SETFL, flags | O_NONBLOCK) != 0)
        LOGI ("%s:%d Couldn't set O_NONBLOCK on %s socket ... proceeding anyway",
              __FUNCTION__, __LINE__, IFNAME);

    memset (&ifr, 0, sizeof (struct ifreq));
    strncpy (ifr.ifr_name, IFNAME, sizeof (IFNAME));
    ifr.ifr_addr.sa_family = AF_INET;

    /* Read Flag for interface usb0, Bit#0 indicates Interface Up/Down */
    if (ioctl (fd, SIOCGIFFLAGS, &ifr) < 0)
        if_status = 0;
    /* if usb0 is up get its ip address */
    else if (ifr.ifr_flags & 0x01)
    {
        if_status = 1;
        ioctl (fd, SIOCGIFADDR, &ifr);
        strncpy (ip_addr_if,
                 (char
                  *)(inet_ntoa (((struct sockaddr_in *) &ifr.ifr_addr)->
                                sin_addr)), 16);
        LOGI ("%s:%d IPAddress: %s", __FUNCTION__, __LINE__, ip_addr_if);
    }
    close (fd);
    return if_status;
}

static int
init_new_file (char *p_input, int rotate_size)
{
    struct stat st;
    int tfd = -1;
    int ret;

	/*debug*/
	LOGI ("%s:%d file %s", __FUNCTION__, __LINE__, p_input);
	
    ret = stat (p_input, &st);
	/*debug*/
	LOGI ("%s:%d file %s stat check ret %d, mode 0x%x, S_ISREG %d, size %lld", __FUNCTION__, __LINE__, p_input, ret, st.st_mode, S_ISREG (st.st_mode), st.st_size);

    if (ret == -1 && errno == ENOENT)
    {
        if ((tfd = open (p_input, O_WRONLY | O_CREAT, S_IRUSR | S_IWUSR)) > 0) {
            if (fallocate (tfd, FALLOC_FL_KEEP_SIZE, 0, rotate_size * 1024) <
                0) {
                LOGE ("%s:%d Error allocating %d KBytes: %s", __FUNCTION__,
                      __LINE__, rotate_size, strerror (errno));
            } else {
				/*debug*/
				LOGI ("%s:%d file %s, tfd %d, fallocate() rotate_size %d KBytes ok", __FUNCTION__, __LINE__, p_input, tfd, rotate_size);
            }
        } else {
			/*debug*/
			LOGI ("%s:%d file %s, open fails", __FUNCTION__, __LINE__, p_input);        
    	}
    }
    else
    {
        LOGD ("%s:%d file %s exists, skipping creation", __FUNCTION__,
              __LINE__, p_input);
        tfd = open (p_input, O_WRONLY);
    }

    return tfd;
}

static char *
get_names (char *p_input, int rotate_size, int rotate_num)
{
    static char *fnames = NULL;

	/*debug*/
	LOGI ("%s:%d file %s, rotate_num %d", __FUNCTION__, __LINE__, p_input, rotate_num);
	
    if (rotate_num != 0 && fnames == NULL)
    {
        /* Initialize fnames and allocates .x files if they don't exists */
        fnames = malloc (rotate_num * (PATH_LEN + 1));
        if (fnames == NULL)
            return NULL;
        memset (fnames, '\0', rotate_num * PATH_LEN + 1);
        strncpy (&fnames[0], p_input, PATH_LEN);
        close (init_new_file (&fnames[0], rotate_size));
        int i;
        for (i = 1; i < rotate_num; i++)
        {
            snprintf (&fnames[i * PATH_LEN], PATH_LEN, "%s.%d.istp", p_input,
                      i);
            LOGD ("%s:%d \t %s", __FUNCTION__, __LINE__,
                  &fnames[i * PATH_LEN]);
            close (init_new_file (&fnames[i * PATH_LEN], rotate_size));
        }
    }
    return fnames;
}

static int
rotateFile (char *p_input, int fs_fd, int rotate_size, int rotate_num)
{
    int err;
    int i;

    /*debug*/
    LOGI ("%s:%d file %s, rotate_size %d, rotate_num %d", __FUNCTION__, __LINE__, p_input, rotate_size, rotate_num);
	
  //  char *fnames = get_names (p_input, rotate_size, rotate_num);

    if (p_input == NULL)
        goto out_err;
   // if (fnames == NULL)
   //     goto out;

    /* close fs_fd in a thread to avoid waiting for a flush on filp_close */
    pthread_t thr_close;
    pthread_attr_t attr;
    pthread_attr_init (&attr);
    pthread_attr_setdetachstate (&attr, PTHREAD_CREATE_DETACHED);
    pthread_create (&thr_close, &attr, (void *) close, (void *) fs_fd);
    pthread_attr_destroy (&attr);
    SLOGI("rotate new path before path=%s",p_input);
    changeNewDatePath(p_input);
    SLOGI("rotate new path after path=%s",p_input);
/*
    for (i = rotate_num - 2; i >= 0; i--)
    {
        err = rename (&fnames[i * PATH_LEN], &fnames[(i + 1) * PATH_LEN]);
        LOGD ("%s:%d renaming log file %s to %s", __FUNCTION__, __LINE__,
              &fnames[i * PATH_LEN], &fnames[(i + 1) * PATH_LEN]);
        if (err < 0 && errno != ENOENT)
            LOGE ("%s:%d \t error: %s", __FUNCTION__, __LINE__,
                  strerror (errno));
    }
*/

out:
	SLOGI("rotate out out new path after path=%s",p_input);
    return init_new_file (p_input, rotate_size);
out_err:
    return -1;
}

static int
get_ldisc_id (const char *ldisc_name)
{
    int val;
    int ret = -1;
    char name[255] = { 0 };
    FILE *fldisc = fopen ("/proc/tty/ldiscs", "r");

    if (fldisc == NULL)
    {
        goto out_noclose;
    }

    while (fscanf (fldisc, "%254s %d", name, &val) == 2)
    {
        /*early return if the ldisc_name is found */
        if (strncmp (ldisc_name, name, strlen (ldisc_name)) == 0)
        {
            ret = val;
            goto out;
        }
    }
out:
    fclose (fldisc);
out_noclose:
    return ret;
}

static int
log_to_pti (char *gsmtty, char *sink, comm_mdm * ctx)
{
    int ldisc_id_sink;
    int ldisc_id_router;
    int ret = -1;
    int rcvd = 0;
    int fs_gsmtty, fs_sink;
    sigset_t sig_mask;

    /* Wait modem ready to start traces */
    if (pthread_mutex_lock (&ctx->cond_mtx) != 0)
    {
        LOGE ("%s:%d PTHREAD_COND_MUTEX Error taking mutex: %s. Exiting wait loop and MTS.",
              __FUNCTION__, __LINE__, strerror (errno));
        goto out;
    }

    while (!ctx->modem_available)
        if (pthread_cond_wait (&ctx->modem_online, &ctx->cond_mtx) != 0)
        {
            LOGE ("%s:%d PTHREAD_COND_WAIT Error: %s. Exiting.", __FUNCTION__,
                  __LINE__, strerror (errno));
            if (pthread_mutex_unlock (&ctx->cond_mtx) != 0)
                LOGE ("%s:%d PTHREAD_COND_MUTEX Error releasing mutex: %s.\
                      Exiting wait loop and MTS.",
                      __FUNCTION__, __LINE__, strerror (errno));
            goto out;
        }

    if (pthread_mutex_unlock (&ctx->cond_mtx) != 0)
    {
        LOGE ("%s:%d PTHREAD_COND_MUTEX Error releasing mutex: %s. Exiting wait loop and MTS.",
              __FUNCTION__, __LINE__, strerror (errno));
        goto out;
    }

    LOGI ("%s:%d opening devices", __FUNCTION__, __LINE__);
    ATTEMPTE (open (gsmtty, O_RDWR), <0, "Opening tty", &fs_gsmtty, out);
    ATTEMPTE (open (sink, O_RDWR), <0, "Opening pti", &fs_sink, out_close_tty);

    LOGI ("%s:%d getting ldiscs", __FUNCTION__, __LINE__);
    /*get ldisc ids */
    ATTEMPTE (get_ldisc_id ("n_tracesink"), == -1, "Getting sink ldisc id",
              &ldisc_id_sink, out_close);
    ATTEMPTE (get_ldisc_id ("n_tracerouter"), == -1, "Getting router ldisc id",
              &ldisc_id_router, out_close);

    LOGI ("%s:%d setting ldiscs", __FUNCTION__, __LINE__);
    /*finally set the ldisc to the devices */
    ATTEMPTE (ioctl (fs_gsmtty, TIOCSETD, &ldisc_id_router), <0,
              "Setting router ldisc", &ret, out_close);
    ATTEMPTE (ioctl (fs_sink, TIOCSETD, &ldisc_id_sink), <0,
              "Setting sink ldisc", &ret, out_rm_ldisc_tty);

    LOGI ("%s:%d waiting for MY_SIG_UNCONF_PTI", __FUNCTION__, __LINE__);
    /*blocks the program, waiting for MY_SIG_UNCONF_PTI signal to happen */
    sigemptyset (&sig_mask);
    sigaddset (&sig_mask, MY_SIG_UNCONF_PTI);
    sigprocmask (SIG_BLOCK, &sig_mask, NULL);
    if (sigwait (&sig_mask, &rcvd) == -1)
        LOGD ("%s:%d ERROR waiting for signal", __FUNCTION__, __LINE__);
    LOGI ("%s:%d Unconfiguring PTI", __FUNCTION__, __LINE__);

    /*deconfigure ldiscs */

    ioctl (fs_sink, TIOCSETD, (int[])
           {
               0
           });
out_rm_ldisc_tty:
    ioctl (fs_gsmtty, TIOCSETD, (int[])
           {
               0
           });
out_close:
    close (fs_sink);
out_close_tty:
    close (fs_gsmtty);
out:
    return ret;
}

static int
init_file (char *p_input, int *rotate_size, int rotate_num)
{
    int ret;
    int fs_fd = -2;
    struct stat st;

	/*debug*/
	LOGI ("%s:%d file %s, rotate_size %d, rotate_num %d", __FUNCTION__, __LINE__, p_input, *rotate_size, rotate_num);

    ret = stat (p_input, &st);
    if (ret == 0 && S_ISREG (st.st_mode))
    {
        /* logs exists so we rotate them so that we start with a fresh bplog */
        LOGI ("%s:%d file %s exists, rotating...", __FUNCTION__, __LINE__,
              p_input);
        fs_fd = rotateFile (p_input, fs_fd, *rotate_size, rotate_num);
    }
    else if (ret == 0 && S_ISCHR (st.st_mode))
    {
        /* We output on a character device, just disable file rotation */
        LOGI ("%s:%d file %s is a Characater device, disabling file rotation...",
              __FUNCTION__, __LINE__, p_input);
        fs_fd = open (p_input, O_WRONLY);
        *rotate_size = 0;
    }
    else if (ret == -1 && errno == ENOENT)
    {
        char *end_dir = strrchr (p_input, '/');

		/*debug*/
		LOGI ("%s:%d file %s, not exist, last / at 0x%x, p_input 0x%x", __FUNCTION__, __LINE__, p_input, end_dir, p_input);

        if (end_dir != NULL)
        {
            *end_dir = '\0';
            if (stat (p_input, &st) == -1 && errno == ENOENT)
            {
                *end_dir = '/';
                /* Creates the missing directories */
                LOGI ("%s:%d Path for file %s does not exists",
                      __FUNCTION__, __LINE__, p_input);
                char path[PATH_LEN];
                char *p = path;
                strncpy (path, p_input, PATH_LEN - 1);
                path[strnlen (p_input, PATH_LEN)] = '\0';
                /* Skips first / to avoid trying to create nothing */
                if (p[0] == '/')
                    p++;
                while ((p = strchr (p, '/')) != NULL)
                {
                    *p = '\0';
                    ret = stat (path, &st);
                    if (ret != 0 && errno == ENOENT)
                    {
                        ret = mkdir (path, 0755);
                        if (ret != 0)
                        {
                            LOGE ("%s:%d Can't create path %s : %s",
                                  __FUNCTION__, __LINE__, path,
                                  strerror (errno));
                            goto out_error;
                        }
                        LOGI ("%s:%d Path %s ... created", __FUNCTION__,
                              __LINE__, path);
                    }
                    *p = '/';
                    p++;
                }
            }
            *end_dir = '/';
        }
        fs_fd = init_new_file (p_input, *rotate_size);
    }
    else
    {
        LOGI ("%s:%d Won't be able to log on %s : %s", __FUNCTION__, __LINE__,
              p_input, strerror (errno));
        goto out_error;
    }
/* add file by record time
    if (rotate_num > 0
        && get_names (p_input, *rotate_size, rotate_num) == NULL)
    {
        LOGE ("%s:%d can't init file list", __FUNCTION__, __LINE__);
        goto out_error;
    }
*/
    if (fs_fd < 0)
    {
        LOGE ("%s:%d Couldn't open %s : %s", __FUNCTION__, __LINE__, p_input,
              strerror (errno));
        goto out_error;
    }

    return fs_fd;
out_error:
    return -1;
}

static int
init_socket (char *ip_addr_if, int port_no)
{
    int connect_fd = -1;
    int sock_stream_fd = -1;
    int ret;
    socklen_t clilen;
    struct sockaddr_in serv_addr, cli_addr;

    LOGI ("%s:%d Open Stream Socket", __FUNCTION__, __LINE__);
    ATTEMPTE (socket (AF_INET, SOCK_STREAM, 0), <0, "Opening stream socket",
              &sock_stream_fd, out);

    memset (&serv_addr, 0, sizeof (serv_addr));
    serv_addr.sin_family = AF_INET;
    inet_aton (ip_addr_if, &serv_addr.sin_addr);
    serv_addr.sin_port = htons (port_no);

    LOGI ("%s:%d Bind to port %d", __FUNCTION__, __LINE__, port_no);
    ATTEMPTE (bind (sock_stream_fd, (struct sockaddr *) &serv_addr,
              sizeof (serv_addr)), <0, "Binding to port", &ret, out);

    LOGI ("%s:%d Listening()...", __FUNCTION__, __LINE__);
    ATTEMPTE (listen (sock_stream_fd, 5), <0, "Listening on sock", &ret, out);

    clilen = sizeof (cli_addr);

    LOGI ("%s:%d Wait for client to connect...", __FUNCTION__, __LINE__);
    ATTEMPTE (accept (sock_stream_fd, (struct sockaddr *) &cli_addr, &clilen),
              <0, "Accepting socket", &connect_fd, out);
    LOGI ("%s:%d Socket %d accepted...", __FUNCTION__, __LINE__, port_no);
out:
    return connect_fd;
}

static void
log_traces (int outfd, char *p_input, char *p_output,
            int rotate_size, int rotate_num, comm_mdm * ctx)
{
    off_t cnt_written = 0;
    int ret = 0;
    bool exit_cond = false;
    int i = 0;
    char trace_buffer[MAXDATA];
    char intercom_msg_buf[MDM_MSG_SZ];
    char inot_event_buf[INOT_BUF_SZ];
    struct pollfd *mtspoll = NULL;

    if ((mtspoll = calloc (POLL_FD_NBR, sizeof (struct pollfd))) == NULL)
    {
        LOGE ("%s:%d POLL struct calloc failed: %s. Exiting wait loop and MTS.",
              __FUNCTION__, __LINE__, strerror (errno));
        return;
    }

    /* Initialize poll event field */
    for (i = 0; i < POLL_FD_NBR; i++)
        mtspoll[i].events = POLLHUP | POLLIN | POLLERR;

    /* Clean any log fd */
    ctx->ttyfd = -1;

    /* Trace until critical error */
    while (!exit_cond)
    {
        /* Wait modem ready to start traces */
        if (pthread_mutex_lock (&ctx->cond_mtx) != 0)
        {
            LOGE ("%s:%d PTHREAD_COND_MUTEX Error taking mutex: %s. Exiting wait loop and MTS.",
                  __FUNCTION__, __LINE__, strerror (errno));
            goto out;
        }

        while (!ctx->modem_available)
        {
            /* Ensure fd is closed - due to previous iteration */
            if (ctx->ttyfd != -1)
                close (ctx->ttyfd);
            if (pthread_cond_wait (&ctx->modem_online, &ctx->cond_mtx) != 0)
            {
                LOGE ("%s:%d PTHREAD_COND_WAIT Error: %s. Exiting.",__FUNCTION__, __LINE__, strerror (errno));
                if (pthread_mutex_unlock (&ctx->cond_mtx) != 0)
                    LOGE ("%s:%d PTHREAD_COND_MUTEX Error releasing mutex: %s.\ Exiting wait loop and MTS.",__FUNCTION__, __LINE__, strerror (errno));
                goto out;
            }
        }

        if (pthread_mutex_unlock (&ctx->cond_mtx) != 0)
        {
            LOGE ("%s:%d PTHREAD_COND_MUTEX Error releasing mutex: %s.\
                  Exiting wait loop and MTS.",
                  __FUNCTION__, __LINE__, strerror (errno));
            goto out;
        }

        /* So far the modem ready event is received - so open must succeed */
        /* Debug trace */
        if (ctx->ttyfd == -1)
            LOGD ("%s:%d Got MODEM_UP, gsmtty to be opened: %s", __FUNCTION__,
                  __LINE__, ctx->p_input);

        if ((ctx->ttyfd == -1)
            && ((ctx->ttyfd = open_gsmtty (ctx->p_input)) < 0))
        {
            LOGE ("%s:%d GSMTTY open failure - but modem is notified UP.\
                  MTS needs to wait for USB enumeration.",
                  __FUNCTION__, __LINE__);
            if (!(ctx->usb_logging))
            {
                LOGE ("%s:%d HSI log port open failure. MTS will exit now.",
                      __FUNCTION__, __LINE__);
                /* For HSI we exit as this is an critical case */
                /* For USB we will get node creation by INOTIFY */
                goto out;
            }
        }

        /* First FD is always the modem log port */
        mtspoll[POLL_FD_LOG].fd = ctx->ttyfd;
        /* 2nd FD pipe read to receive modem event */
        mtspoll[POLL_FD_COM].fd = ctx->intercom[0];
        /* If 3rd FD, this is the USB INOTIFIER */
        if (ctx->usb_logging)
            mtspoll[POLL_FD_NOT].fd = ctx->inotfd;
        else
            mtspoll[POLL_FD_NOT].fd = -1;

        /* Poll FDs */
        ret = poll (mtspoll, POLL_FD_NBR, -1);
        if (ret < 0 && ret != EINVAL)
        {
            LOGD ("%s:%d Interrupted poll - reason: %s. MTS will continue.",
                  __FUNCTION__, __LINE__, strerror (errno));
            continue;
        }
        if (ret < 0 && ret == EINVAL)
        {
            LOGE ("%s:%d Error in poll - reason: %s. MTS will exit.",
                  __FUNCTION__, __LINE__, strerror (errno));
            goto out;
        }

        /* process poll data */
        for (i = 0; i < POLL_FD_NBR; i++)
        {

            if (mtspoll[i].revents & POLLIN)
            {
                int n, n1;
                switch (i)
                {

                /* CASE POLL_FD_LOG */

                case POLL_FD_LOG:

                    memset (trace_buffer, 0, sizeof (trace_buffer));
                    n = n1 = 0;

                    ATTEMPTE (read (mtspoll[POLL_FD_LOG].fd, trace_buffer,
                              MAXDATA), <0, "Reading input data", &n,
                              out);

                    /* Workaround dlp_trace bug */
                    if (n == 0)
                    {
                        if (ctx->usb_logging)
                        {
                            LOGD ("%s:%d Warning: READ 0 bytes on an invalid fd.\
                                  Will close and reopen it.",
                                  __FUNCTION__, __LINE__);
                            close (ctx->ttyfd);
                            ctx->ttyfd = -1;
                        }
                        break;
                    }
                    /*
                    ATTEMPTE (write (outfd, trace_buffer, n),
                              <=0
                              && errno != ENOSPC, "Writing data to output",
                              &n1, out);*/

                    for(;;){ //system
			n1=write (outfd, trace_buffer, n);
			if(n1<0){
				if(errno==EINTR){//retry when Interrupted system call,write fail
					SLOGI("wait write ready!!!");
					usleep(10);
					continue;
				}else{
					char* error=strerror(errno);
					SLOGI("error=%s",error);
					goto out;
				     }
				}else{
					break;
				}
                    }


                    if (n1 <= 0)
                    {
                        /* if we end up with no space to write on device, then we have a problem.
                           we suppose that pre-allocation didn't work (not implemented on FS for
                           example) mts can't just exit because android will restart the service
                           and chaos will ensue.
                           Cleaner solution is to divide rotate_size by 1.5 and
                           force a file rotation in the hope to free enough space to keep logging.
                         */
                        if (rotate_size > 1024)
                        {
                            rotate_size /= 1.5;
                            LOGD ("%s:%d Rotate Size adjusted to: %d",
                                  __FUNCTION__, __LINE__, rotate_size);
                            ATTEMPTE (rotateFile(p_output, outfd, rotate_size,
                                      rotate_num), <0, "Rotating logs",
                                      &outfd, out);
                            cnt_written = 0;
                            break;
                        }
                        /* if we end up there, there's not much we can do ...
                           just pause the logging :(
                         */
                        else
                        {
                            LOGE ("%s:%d No more space on device!",
                                  __FUNCTION__, __LINE__);
                            pause ();
                        }
                    }

                    cnt_written += n;
                    if (rotate_size > 0
                        && (cnt_written / 1024) >= rotate_size)
                    {
                        ATTEMPTE (rotateFile(p_output, outfd, rotate_size,
                                  rotate_num), <0, "Rotating logs",
                                  &outfd, out);
                        cnt_written = 0;
                        LOGI ("%s:%d Logs rotated", __FUNCTION__,
                              __LINE__);
                    }
                    break;

                /* CASE POLL_FD_COM */

                case POLL_FD_COM:

                    LOGD ("%s:%d Get modem event notification while polling.",
                          __FUNCTION__, __LINE__);
                    memset (intercom_msg_buf, 0,
                            sizeof (intercom_msg_buf));
                    n = n1 = 0;

                    ATTEMPTE (read(mtspoll[POLL_FD_COM].fd, intercom_msg_buf,
                              MDM_MSG_SZ * sizeof (char)), <0,
                              "Reading modem event data", &n, out);

                    if (strncmp(intercom_msg_buf, MDM_UP,
                        MDM_MSG_SZ * sizeof (char)) == 0)
                    {
                        LOGD ("%s:%d Get modem event notification - MODEM UP.",
                              __FUNCTION__, __LINE__);
                    }
                    if (strncmp(intercom_msg_buf, MDM_DW,
                        MDM_MSG_SZ * sizeof (char)) == 0)
                    {
                        LOGD ("%s:%d Get modem event notification - MODEM DOWN.",
                              __FUNCTION__, __LINE__);
                        /* Close tty */
                        if (ctx->ttyfd != -1)
                        {
                            LOGD ("%s:%d Log FD closed.", __FUNCTION__,
                                  __LINE__);
                            close (ctx->ttyfd);
                            ctx->ttyfd = -1;
                        }
                    }

                    break;

                /* CASE POLL_FD_NOT */

                case POLL_FD_NOT:

                    memset (inot_event_buf, 0, sizeof (inot_event_buf));
                    n = n1 = 0;

                    ATTEMPTE (read(mtspoll[POLL_FD_NOT].fd, inot_event_buf,
                              INOT_BUF_SZ), <0, "Reading inotify data",
                              &n, out);

                    if (n == 0)
                    {
                        LOGD ("%s:%d Warning: READ 0 bytes for inotify event.",
                              __FUNCTION__, __LINE__);
                        break;
                    }

                    while (n1 < n)
                    {
                        struct inotify_event *evt =
                            (struct inotify_event *) &inot_event_buf[n1];
                        struct timespec ptim;
                        ptim.tv_sec = 0;
                        ptim.tv_nsec = 50000000L;       /* 50 ms sleep */
                        if (evt->len)
                        {
                            if ((evt->mask & IN_CREATE)
                                && (!(evt->mask & IN_ISDIR)))
                            {
                                int attempt = 5;
                                bool try = true;
                                LOGD ("%s:%d USB node %s CREATED.",
                                      __FUNCTION__, __LINE__,
                                      evt->name);
                                /* Give time for ttyACM1 node creation */
                                nanosleep (&ptim, NULL);
                                if (ctx->ttyfd == -1)
                                {
                                    LOGD ("%s:%d MTS will loop until we connect on USB.",
                                          __FUNCTION__, __LINE__);
                                    while ((ctx->ttyfd =
                                                open_gsmtty (ctx->
                                                             p_input)) < 0 && try)
                                    {
                                        if (!attempt)
                                        {
                                            LOGE ("%s:%d Too many open tentative. \
                                            MTS will give up until next USB event.",
                                            __FUNCTION__, __LINE__);
                                            n1 += INOT_EVENT_SZ + evt->len;
                                            try = false;
                                        }
                                        nanosleep (&ptim, NULL);
                                        attempt--;
                                    }
                                }
                                n1 += INOT_EVENT_SZ + evt->len;
                            }
                            if ((evt->mask & IN_DELETE)
                                && (!(evt->mask & IN_ISDIR)))
                            {
                                LOGD ("%s:%d USB node %s DELETED.",
                                      __FUNCTION__, __LINE__,
                                      evt->name);
                                if (ctx->ttyfd != -1)
                                {
                                    LOGD ("%s:%d USB node FD closed.",
                                          __FUNCTION__, __LINE__);
                                    close (ctx->ttyfd);
                                    ctx->ttyfd = -1;
                                }
                                n1 += INOT_EVENT_SZ + evt->len;
                            }
                        }
                    }
                    break;
                }
            }
            if (mtspoll[i].revents & POLLERR)
            {
                switch (i)
                {
                case POLL_FD_LOG:
                    LOGE ("%s:%d Error: POLLERR event captured on fd. Closing LOG fd.",
                      __FUNCTION__, __LINE__);
                    if (ctx->ttyfd != -1)
                    {
                        close (ctx->ttyfd);
                        ctx->ttyfd = -1;
                    }
                    break;
                case POLL_FD_COM:
                case POLL_FD_NOT:
                    LOGE ("%s:%d Error: POLLERR event captured on fd. Exiting MTS.",
                      __FUNCTION__, __LINE__);
                    goto out;
                    break;
                }
            }
            if (mtspoll[i].revents & POLLHUP)
            {
                switch (i)
                {
                case POLL_FD_LOG:
                    LOGD ("%s:%d Warning: POLLHUP event captured on fd. Closing LOG fd.",
                      __FUNCTION__, __LINE__);

                    if (ctx->ttyfd != -1)
                    {
                        close (ctx->ttyfd);
                        ctx->ttyfd = -1;
                    }
                    break;
                case POLL_FD_COM:
                case POLL_FD_NOT:
                    LOGD ("%s:%d Warning: POLLERR event captured on fd. Exiting MTS.",
                      __FUNCTION__, __LINE__);
                    goto out;
                    break;
                }
            }
        }

        /* Clean up revents fields - everything is processed */
        for (i = 0; i < POLL_FD_NBR; i++)
            mtspoll[i].revents = 0;

    }

out:
    if (ctx->ttyfd != -1)
    {
        close (ctx->ttyfd);
        ctx->ttyfd = -1;
    }
    close (outfd);
    if (mtspoll != NULL)
        free (mtspoll);
    LOGD ("%s:%d Closed input: %s and output: %s", __FUNCTION__, __LINE__,
          p_input, p_output);
    return;
}

int
mdm_up (mmgr_cli_event_t * ev)
{
    comm_mdm *ctx = (comm_mdm *) ev->context;
    LOGD ("%s:%d Received MODEM_UP", __FUNCTION__, __LINE__);

    if (pthread_mutex_lock (&ctx->cond_mtx) != 0)
    {
        LOGE ("%s:%d PTHREAD_COND_MUTEX Error taking mutex: %s. CRITICAL: PTHREAD_COND not sent!",
              __FUNCTION__, __LINE__, strerror (errno));
        goto out;
    }
    /* Modem up - start tracing */
    ctx->modem_available = true;
    if (write (ctx->intercom[1], MDM_UP, MDM_MSG_SZ * sizeof (char))
        != (MDM_MSG_SZ * sizeof (char)))
        LOGE ("%s:%d Modem event msg not properly sent. MTS may missbehave.",
             __FUNCTION__, __LINE__);

    if (pthread_mutex_unlock (&ctx->cond_mtx) != 0)
    {
        LOGE ("%s:%d PTHREAD_COND_MUTEX Error releasing mutex: %s.\
              CRITICAL: MTS not expected to work anymore. PTHREAD_COND not sent!",
              __FUNCTION__, __LINE__, strerror (errno));
        goto out;
    }
    /* Send signal to start tracing */
    if (pthread_cond_signal (&ctx->modem_online) != 0)
        LOGE ("%s:%d PTHREAD_COND_SIGNAL Error: %s.\
               CRITICAL: MTS not expected to work anymore. PTHREAD_COND not sent!",
              __FUNCTION__, __LINE__, strerror (errno));
out:
    return 0;
}

int
mdm_dwn (mmgr_cli_event_t * ev)
{
    comm_mdm *ctx = (comm_mdm *) ev->context;
    LOGD ("%s:%d Received MODEM_DOWN", __FUNCTION__, __LINE__);

    if (pthread_mutex_lock (&ctx->cond_mtx) != 0)
    {
        LOGE ("%s:%d PTHREAD_COND_MUTEX Error taking mutex: %s. \
              CRITICAL: MTS not expected to work anymore. Trouble on mdm_down notification !",
              __FUNCTION__, __LINE__, strerror (errno));
        goto out;
    }
    /* Modem down - stop tracing */
    ctx->modem_available = false;
    if (write (ctx->intercom[1], MDM_DW, MDM_MSG_SZ * sizeof (char))
        != (MDM_MSG_SZ * sizeof (char)))
        LOGE ("%s:%d Modem event msg not properly sent. MTS may missbehave.",
             __FUNCTION__, __LINE__);

    if (pthread_mutex_unlock (&ctx->cond_mtx) != 0)
        LOGE ("%s:%d PTHREAD_COND_MUTEX Error releasing mutex: %s. \
              CRITICAL: MTS not expected to work anymore. Trouble on mdm_down notification t!",
              __FUNCTION__, __LINE__, strerror (errno));

out:
    return 0;
}

void getDir(char * result,const char * pPath){
	char * pFilename=strrchr(pPath,(int)'/');
	strncpy(result, pPath, pFilename-pPath);
	strcat(result,"/");

}

void changeNewDatePath(char * p_output){
	char * pFilename=strrchr(p_output,(int)'/');
	char szDir[PATH_SIZE]={0};
	strncpy(szDir, p_output, pFilename-p_output);
	strcat(szDir,"/");
	char storePath[PATH_SIZE]={0};
	SLOGI("path=%s",p_output);
	onHandelLogPath(false, storePath,szDir,FILE_NAME_BPLOG );
	strcpy(p_output, storePath);
	/*debug*/
	LOGI ("%s:%d final path=%s", __FUNCTION__, __LINE__, p_output);
}

bool checkFileExist(char *p_input ){
	 struct stat st;
	 int input_ret = stat (p_input, &st);
	 SLOGI("modem port status!!! input_ret result=%d,p_input=%s",input_ret,p_input);
	 if(input_ret==0){
		 return true;
	 }
	 return false;
}


void sendErrorMessage(char* msg,int mode){
	LOGE(msg, __FUNCTION__, __LINE__);
	char logdate[80]={0};
	char szCmd[260]={0};
	getDate(logdate,80);
	switch(mode){
	case INIT_FAIL:
		sprintf(szCmd,"echo INIT_FAIL   		> fail_%s.dump && echo %s >> fail_%s.dump",logdate,msg,logdate);
		system(msg);
		system("am broadcast -a com.asus.modemlog.initfail");
		break;
	case COND_FAIL:
		sprintf(szCmd,"echo COND_FAIL   		> fail_%s.dump && echo %s >> fail_%s.dump",logdate,msg,logdate);
		system(msg);
		break;
	case MUTEX_FAIL:
		sprintf(szCmd,"echo MUTEX_FAIL  		> fail_%s.dump && echo %s >> fail_%s.dump",logdate,msg,logdate);
		system(msg);
		break;
	case SEARIL_PORT_NULL:
		sprintf(szCmd,"echo SEARIL_PORT_NULL   > fail_%s.dump && echo %s >> fail_%s.dump",logdate,msg,logdate);
		system(msg);
		system("am broadcast -a com.asus.modemlog.portfail");
		break;
	}



}

int
main (int argc, char **argv)
{
    char ip_addr_if[16] = "";
    int ret = 0;
    struct comm_mdm_s ctx;
    int outfd = -1;

    char p_output[PATH_LEN] = { 0 };
    char p_out_type = '\0';
    char p_input[PATH_LEN] = { 0 };
    int p_rotate_size = DEFAULT_LOG_SIZE;
    int p_rotate_num = DEFAULT_MAX_ROTATED_NR;
    unsigned int i, j;

    ctx.ttyfd = -1;
    ctx.inotfd = -1;
    ctx.inotwd = -1;
    ctx.modem_available = false;
    ctx.usb_logging = false;
    struct args_s list[] = {{ .name = PROP_HEAD "output",.key =
                                "-o",.type_conv = "%s",.storage = p_output},
                          { .name = PROP_HEAD "output_type",.key = "-t",.type_conv = "%c",.storage =
                                &p_out_type},
                          { .name = PROP_HEAD "input",.key = "-i",.type_conv = "%s",.storage =
                                p_input},
                          { .name = PROP_HEAD "rotate_size",.key = "-r",.type_conv = "%d",.storage =
                                &p_rotate_size},
                          { .name = PROP_HEAD "rotate_num",.key = "-n",.type_conv = "%d",.storage =
                                &p_rotate_num}};
    char key_array[sizeof (list) / sizeof (*list)] = { 0 };

    LOGI ("%s Version:%s%s", argv[0], __DATE__, __TIME__);
    char dump[260]={0};
    if (pthread_cond_init (&ctx.modem_online, NULL) != 0)
    {
    	 sprintf(dump,"%s:%d modem_online PTHREAD_COND cannot be initialized: error %s. Exiting.",
                 __FUNCTION__, __LINE__, strerror (errno));
    	sendErrorMessage(dump,COND_FAIL);
        goto cond_failure;
    }

    if (pthread_mutex_init (&ctx.cond_mtx, NULL) != 0)
    {
    	sprintf(dump,"%s:%d PTHREAD_COND_MUTEX cannot be initialized: error %s. Exiting.",
                __FUNCTION__, __LINE__, strerror (errno));
    	sendErrorMessage(dump,MUTEX_FAIL);
    	goto mtx_failure;
    }

    if (pipe (ctx.intercom) == -1)
    {

        sprintf(dump,"%s:%d PIPE for intercom cannot be initialized: error %s. Exiting.",
                __FUNCTION__, __LINE__, strerror (errno));

        sendErrorMessage(dump,INIT_FAIL);
        goto init_failure;
    }

    if (argc > 1)
    {
        for (i = 1; i < (unsigned) argc; i++)
        {
            for (j = 0; j < sizeof (list) / sizeof (*list); j++)
            {
                if (key_array[j] == 1)
                    continue;
                if (strncmp (list[j].key, argv[i], 2) == 0)
                {
                    sscanf (argv[++i], list[j].type_conv, list[j].storage);
                    key_array[j] = 1;
                    break;
                }
            }
        }

    }
    else
    {
        char result[92];
        for (i = 0; i < sizeof (list) / sizeof (*list); i++)
        {
            property_get (list[i].name, result, "");
            if (result[0] == '\0')
                continue;
            sscanf (result, list[i].type_conv, list[i].storage);
        }
    }
    LOGD ("%s:%d Parameters: out_type: %c output: %s input: %s rotate_num: %d rotate_size: %d",
          __FUNCTION__, __LINE__, p_out_type, p_output, p_input, p_rotate_num,
          p_rotate_size);
    if (!p_input[0] || !p_output[0])
    {
        puts (USAGE_TXT);
        goto init_failure;
    }

    if (strncmp (p_input, USB_LOG_DEV, sizeof (USB_LOG_DEV)) == 0)
    {
        LOGD ("%s:%d: USB logging enabled - activate INOTIFY watch.",
              __FUNCTION__, __LINE__);
        ctx.usb_logging = true;
        /* Create INOTIFY instance */
        if ((ctx.inotfd = inotify_init ()) < 0)
        {

            sprintf(dump,"%s:%d  INOTIFY cannot be initialized: error %s. Exiting.",
                    __FUNCTION__, __LINE__, strerror (errno));
            sendErrorMessage(dump,INIT_FAIL);
            goto init_failure;
        }

        /* Add watch for USB device */
        if ((ctx.inotwd =
                 inotify_add_watch (ctx.inotfd, INOT_USB_DEV_NODE,
                                    IN_CREATE | IN_DELETE)) < 0)
        {

            sprintf(dump,"%s:%d  INOTIFY WATCH cannot be initialized: error %s. Exiting.",
                    __FUNCTION__, __LINE__, strerror (errno));
            sendErrorMessage(dump,INIT_FAIL);
            goto inotwatch_failure;
        }
    }

    ctx.mmgr_hdl = NULL;
    mmgr_cli_create_handle (&ctx.mmgr_hdl, "mts", &ctx);
    mmgr_cli_subscribe_event (ctx.mmgr_hdl, mdm_up, E_MMGR_EVENT_MODEM_UP);
    mmgr_cli_subscribe_event (ctx.mmgr_hdl, mdm_dwn, E_MMGR_EVENT_MODEM_DOWN);
    ctx.p_input = p_input;

    uint32_t iMaxTryConnect = MAX_WAIT_MMGR_CONNECT_SECONDS * 1000 / MMGR_CONNECT_RETRY_TIME_MS;

    while (iMaxTryConnect-- != 0) {

        /* Try to connect */
        ret = mmgr_cli_connect (ctx.mmgr_hdl);

        if (ret == E_ERR_CLI_SUCCEED) {

            break;
        }

        ALOGE("Delaying mmgr_cli_connect %d", ret);

        /* Wait */
        usleep(MMGR_CONNECT_RETRY_TIME_MS * 1000);
    }
    /* Check for unsuccessfull connection */
    if (ret != E_ERR_CLI_SUCCEED) {

        ALOGE("Failed to connect to mmgr %d", ret);

    }
    char modemDir[260]={0};
    char szCmd[260]={0};

    getDir(modemDir,p_output);
    switch (p_out_type)
       {
       case 'f':
        /* disables rotation if one or the other param is <=0 */
        if (p_rotate_num <= 0 || p_rotate_size <= 0)
            p_rotate_size = p_rotate_num = 0;
        SLOGI("check mount");

        sprintf(szCmd,"echo result > %s",p_output);
        int result=system(szCmd);
        SLOGI("check mount result=%d",result);
        if(result!=0){
        	exit(-1);//server restart ,util system mount
        }else{
        	 sprintf(szCmd,"rm -rf %s",p_output);
        	 system(szCmd);
        	 SLOGI("check mount ok");
        }

        // check p_input exist /dev/mdmTrace
       if(checkFileExist(p_input)==false){
    	   sprintf(dump,"/dev/mdmTrace no exist");
    	   sendErrorMessage(dump,SEARIL_PORT_NULL);
    	   exit(-1);
       };


        changeNewDatePath( p_output);
        outfd = init_file (p_output, &p_rotate_size, p_rotate_num);

        SLOGI("open file outfd=%d",outfd);
        if(outfd==-1){
        	SLOGI("open file fail");
        	exit(-1);
        }
        log_traces (outfd, p_input, p_output, p_rotate_size, p_rotate_num, &ctx);
        break;
    case 'p':
        while (!iface_up (ip_addr_if))
        {
            sleep (CHECK_INTERVAL);
            LOGI ("%s:%d wait iface_up(%s)", __FUNCTION__, __LINE__, IFNAME);
        }
        LOGI ("%s:%d %s is up, do init_socket()", __FUNCTION__, __LINE__,
              IFNAME);
        outfd = init_socket (ip_addr_if, atoi (p_output));
        log_traces (outfd, p_input, NULL, 0, 0, &ctx);
        break;
    case 'k':
        /* log_to_pti will sets pti sink/sources and block until
           a signal MY_SIG_UNCONF_PTI is received
           then it will clean and close all fds
         */
        log_to_pti (p_input, p_output, &ctx);
        break;
    default:
        puts (USAGE_TXT);
    }

    mmgr_cli_disconnect (ctx.mmgr_hdl);
    mmgr_cli_delete_handle (ctx.mmgr_hdl);
    if (ctx.usb_logging)
        inotify_rm_watch (ctx.inotfd, ctx.inotwd);
inotwatch_failure:
    if (ctx.usb_logging)
        close (ctx.inotfd);
    close (ctx.intercom[0]);
    close (ctx.intercom[1]);
init_failure:

    pthread_mutex_destroy (&ctx.cond_mtx);
mtx_failure:
    pthread_cond_destroy (&ctx.modem_online);
cond_failure:
    LOGI ("%s:%d Exiting mts", __FUNCTION__, __LINE__);

    return 0;
}


