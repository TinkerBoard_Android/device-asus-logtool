package com.asus.tool;

import java.io.File;

import android.app.Service;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.res.AssetManager;
import android.os.IBinder;
import android.os.SystemProperties;
import android.util.Log;

public class LogInitService extends Service{

	private static final String TAG = "LogInitService";

	@Override
	public IBinder onBind(Intent arg0) {
		
		return null;
	}

	public static synchronized void initFile(Context context){

		String files[] =new String[]{"tcpdump","procrank","asuslogcat","init_fdc_logtool.sh","busybox","packlogs.sh","logdumps","init_log_date.sh"};

		ContextWrapper c = new ContextWrapper(context);

		//log("data file="+c.getFilesDir().getAbsolutePath()+",c.getFilesDir().exists()="+c.getFilesDir().exists());

		String datadir=c.getFilesDir().getAbsolutePath();
		File fileInitCheck=new File(datadir+"/"+"init_fdc_logtool.sh");
		File dateCheck=new File(datadir+"/"+"init_log_date.sh");
		if(fileInitCheck.exists() && dateCheck.exists())
		{
			return;
		}
		
		if(c.getFilesDir().exists()==false){
			c.getFilesDir().mkdirs();
		}

		for(String name:files){
			File file =new File(datadir+"/"+name);
			if(file.exists()==false){
				Util.copyAssetFile(context.getAssets(), context, name, datadir+"/"+name);
			}

		}

		SystemProperties.set("ctl.start","logtoolinit");
		
	}
	
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		Log.v(TAG, "onStartCommand");
		initFile(this);
		stopSelf();
		return START_NOT_STICKY;
	}

	
	
	
}
