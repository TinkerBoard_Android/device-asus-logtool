package com.asus.log;

import android.app.Activity;
import android.util.Log;
import android.view.View;
import android.widget.Switch;

import com.asus.fdclogtool.R;

public class BluetoothLog extends BaseLog{

	private static final String TAG = "BluetoothLog";
	private Switch mBlueSwitch;
	public BluetoothLog(Activity activity,View view)
	{
		super(activity, view);
		mBlueSwitch=(Switch) view.findViewById(R.id.switch_bluetooth_id);
		
	}
	
	

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onSelectAll() {
		// TODO Auto-generated method stub
		super.onSelectAll();
		mBlueSwitch.setChecked(true);
	}

	@Override
	public void onCancelAll() {
		// TODO Auto-generated method stub
		super.onCancelAll();
		mBlueSwitch.setChecked(false);
	}
	
	public static void log(String message){
		Log.v(TAG, message);
	}
}
