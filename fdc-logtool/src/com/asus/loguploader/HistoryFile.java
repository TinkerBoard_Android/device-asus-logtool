package com.asus.loguploader;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.text.format.Time;
import android.util.Log;

public class HistoryFile {
    public static final String TAG = "LogFile";

    private Context mContext;
    HistoryFile(Context context) {
        mContext = context;
    }

    public void writeDebugLog(String content) {
        String dir = getDataDirectory();
        try {
            File fileName=new File(dir +"/log.txt");
            FileWriter FW = new FileWriter(fileName, true);
            BufferedWriter BW = new BufferedWriter(FW);
            String temp = null;
            BW.write(getCurrentTime());
            BW.write(content);
            BW.newLine();
            BW.flush();
            BW.close();
        }
        catch(Exception e) {
            System.out.println(e.toString());
        }
    }

    private String getCurrentTime() {
        Time t = new Time();
        t.setToNow();
        String current = "";

        String Month ="";
        String MonthDay ="";
        String Hour="";
        String Min="";
        Month = appendZeroIfNecessary(t.month+1);
        MonthDay = appendZeroIfNecessary(t.monthDay);
        Hour = appendZeroIfNecessary(t.hour);
        Min = appendZeroIfNecessary(t.minute);

        current = Integer.toString(t.year) + "/" + Month + "/" + MonthDay + " " + Hour + ":" + Min + " ";
        return current;
    }

    private String appendZeroIfNecessary(int i){
        String s;
        if (i < 10)
            s = "0" + Integer.toString(i);
        else
            s = Integer.toString(i);

        return s;
    }

    private String getDataDirectory(){
        PackageManager m = mContext.getPackageManager();
        String s = mContext.getPackageName();
        try {
            PackageInfo p = m.getPackageInfo(s, 0);
            s = p.applicationInfo.dataDir;
        } catch (NameNotFoundException e) {
            Log.d(TAG, "Error Package name not found ", e);
        }

        return s;
    }
}