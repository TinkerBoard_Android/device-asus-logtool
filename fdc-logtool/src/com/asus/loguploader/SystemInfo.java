package com.asus.loguploader;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.SystemProperties;
import android.telephony.TelephonyManager;

public class SystemInfo{

    private static final String ACTION_PADDOCK_VERSION ="com.asus.paddock.action.VERSION";
    private static final String ACTION_CAMERA_VERSION = "com.asus.camera.action.VERSION";
    private static final String ACTION_PADDOCK_COMPOSEDVERSION ="com.asus.paddock.action.COMPOSEDVERSION";

    private Context mContext;

    public String mModel;
    public String mIMEINumber;
    public String mFirmwareVersion;
    public String mCameraVersion = "";  // assign default value by Joey_Lee
    public String mStationVersion = "";

    public SystemInfo(Context context){
        this.mContext = context;
        mModel = SystemProperties.get("ro.build.product","");
        getIMEI();
        mFirmwareVersion = SystemProperties.get("ro.build.display.id","");
//      registerCameraFWEvent();  // project dependency by Joey_Lee
//      registerPadDockEvent();
    }

    private void getIMEI(){
        TelephonyManager telephonyManager = (TelephonyManager)mContext.getSystemService(Context.TELEPHONY_SERVICE);
        mIMEINumber = telephonyManager.getDeviceId();
    }

//    private void registerCameraFWEvent() {  // project dependency by Joey_Lee
//        IntentFilter filter = new IntentFilter();
//        filter.addAction(ACTION_CAMERA_VERSION);
//        Intent intent = mContext.registerReceiver(mCameraFWReceiver, filter);
//        if (intent != null) {
//          mCameraVersion = intent.getStringExtra(Intent.EXTRA_CAMERA_VERSION);
//        }else {
//          mCameraVersion = "";
//        }
//    }

//    private void registerPadDockEvent() {  // project dependency by Joey_Lee
//        IntentFilter filter = new IntentFilter();
//        //filter.addAction(Intent.ACTION_PADDOCK_EVENT);
//        //filter.addAction(ACTION_PADDOCK_VERSION);
//        filter.addAction(ACTION_PADDOCK_COMPOSEDVERSION);
//        Intent intent = mContext.registerReceiver(mPadDockReceiver, filter);
//        if (intent != null) {
//          mStationVersion = intent.getStringExtra(Intent.EXTRA_PADDOCK_NAME);
//          if(mStationVersion == null)
//              mStationVersion = "";
//        }else{
//          mStationVersion = "";
//        }
//    }

    private final BroadcastReceiver mCameraFWReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
        }
    };

    private final BroadcastReceiver mPadDockReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
        }
    };

}