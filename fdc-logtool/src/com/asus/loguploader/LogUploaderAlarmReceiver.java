package com.asus.loguploader;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
public class LogUploaderAlarmReceiver extends BroadcastReceiver {
    public static final String TAG = "LogUploaderService";
    public void onReceive(Context context, Intent intent) {
        Log.d(TAG, "in AsusUserTrailAlarmReceiver");
        String upload_action;
        upload_action = intent.getStringExtra("upload-action");
        Log.d(TAG, "upload-action = " + upload_action);
        Intent myintent = new Intent(upload_action);
        myintent.setClass(context,LogUploaderService.class);
        context.startService(myintent);
    }
}