package com.asus.loguploader;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.os.FileObserver;
import android.os.SystemClock;
import android.util.Log;
import android.os.SystemProperties;
import android.view.InputEvent;
import android.view.KeyEvent;
import android.hardware.input.InputManager;

public class CombineKeyReceiver extends BroadcastReceiver {
    public static final String TAG = "CombineKeyReceiver";
//    private ScreenshotObserver mScreenshotFileObserver;
    public static final int INJECT_INPUT_EVENT_MODE_ASYNC = 0;
    public static final int KEYCODE_CAPTURE         = 229;

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        Log.d(TAG, "BootReceiver onReceive action: " + action);

        if (SystemProperties.getInt("persist.asus.mupload.enable", 0) != 1)
            return;

        if ("com.asus.loguploader.action.COMBINE_KEY".equals(action)) {
            // disable unstable ScreenshotObserver and move the copy screenshot.jpg function into script by joey_lee@asus.com
//            mScreenshotFileObserver = new ScreenshotObserver(Environment.getExternalStorageDirectory().getPath()+"/Screenshots");
//            mScreenshotFileObserver.startWatching();
            boolean isScreenshot = sendKeyEvent(new KeyEvent(KeyEvent.ACTION_DOWN,KEYCODE_CAPTURE));
            Log.d(TAG, "CombineKeyReceiver start activity (disable ScreenshotFileObserver)");
            Intent i = new Intent(context, MyTabActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(i);
        }
    }

    private boolean sendKeyEvent(KeyEvent key) {
        try {
            //if (DEBUG) Slog.d(TAG, "injecting key event: " + key);
        	
//        	if (InputManager.getInstance().injectInputEvent(key, INJECT_INPUT_EVENT_MODE_ASYNC) == true) {
//                return true;
//            }
        	if(injectInputEventmethod( key, INJECT_INPUT_EVENT_MODE_ASYNC)){
        		return true;
        	} 
        	
        } catch (Exception ex) {
        }
        return false;
    }

    
    boolean injectInputEventmethod(KeyEvent key,int value)
    {
    	Method getInstance;
  	    InputManager inputManager=null;
  	    boolean result=false;
  		try {
  			getInstance = InputManager.class.getMethod("getInstance");
  			 try {
  				inputManager= (InputManager) getInstance.invoke(null );
  				Method injectInputEventmethod=inputManager.getClass().getMethod("injectInputEvent",InputEvent.class,int.class);
  				result=(Boolean) injectInputEventmethod.invoke(inputManager,key,value);
  			 } catch (IllegalArgumentException e) {
  				// TODO Auto-generated catch block
  				e.printStackTrace();
  				 Log.v(TAG, "IllegalArgumentException");
  			} catch (IllegalAccessException e) {
  				// TODO Auto-generated catch block
  				e.printStackTrace();
  				 Log.v(TAG, "IllegalAccessException");
  			} catch (InvocationTargetException e) {
  				// TODO Auto-generated catch block
  				e.printStackTrace();
  				 Log.v(TAG, "InvocationTargetException");
  			}
  		} catch (NoSuchMethodException e) {
  			// TODO Auto-generated catch block
  			e.printStackTrace();
  			 Log.v(TAG, "NoSuchMethodException");
  		}
  		return result;
    }
}
