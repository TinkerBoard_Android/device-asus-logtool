package com.asus.loguploader;

import android.os.FileObserver;

public class ScreenshotObserver  extends FileObserver{
    public ScreenshotObserver(String path) {
        super(path);
        // TODO Auto-generated constructor stub
    }
    @Override
    public void onEvent(int event, String path) {
        final int action = event & FileObserver.ALL_EVENTS;
        switch (action) {

        case FileObserver.OPEN:
            break;
        case FileObserver.CLOSE_WRITE:

            //parsing file type: jpg or png
            String[] token = path.split("\\.");
            String moveFileCmd = "mv /sdcard/Screenshots/"+ path +" /sdcard/ASUS/LogUploader/screenshot."+token[1];
            try{
                Runtime.getRuntime().exec(moveFileCmd);

            }catch(Exception e){
                e.printStackTrace();
            }
            this.stopWatching();
            break;
        }
    }

}
