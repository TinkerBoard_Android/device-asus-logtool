package com.asus.loguploader;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class ResetAutoUploadTimeReceiver extends BroadcastReceiver {
    public static final String TAG = "ResetAutoUploadTimeReceiver";
    public void onReceive(Context context, Intent intent) {
        Log.d(TAG, "onReceive");
        Intent myintent = new Intent("reset-autoupload");
        myintent.putExtra("resetTime", intent.getStringExtra("resetTime"));
        myintent.setClass(context,LogUploaderService.class);
        context.startService(myintent);
    }
}