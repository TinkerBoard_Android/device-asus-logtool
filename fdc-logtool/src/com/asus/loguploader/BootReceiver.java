package com.asus.loguploader;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class BootReceiver extends BroadcastReceiver {
    public static final String TAG = "LogUploaderService";

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        Log.d(TAG, "BootReceiver onReceive action: " + action);

        if (Intent.ACTION_BOOT_COMPLETED.equals(action)) {
            Log.d(TAG, "Start up AsusUserTrailService by invoking startService");
            Intent inntent = new Intent(context, LogUploaderService.class);
            context.startService(inntent);
        }
    }

}
