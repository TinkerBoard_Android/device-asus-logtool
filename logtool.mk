LOCAL_PATH =device/asus/logtool

PRODUCT_COPY_FILES += \
        $(LOCAL_PATH)/init.logtool.rc:root/init.logtool.rc \

PRODUCT_PACKAGES += \
		  libdumpsys_logtool \
		  logcommand \
		  DetectLogApp \
		  tool_connect \
		  asuslogcat \
		  LogUploader \
		  init_log_date.sh \
		  logkmsg \
		  logdumps \
		  packlogs.sh

TARGET_LOG_TOOL := "false"
#ifeq ($(filter $(TARGET_BUILD_VARIANT), user), )
#TARGET_LOG_TOOL :="true"
#endif


ifeq ($(findstring $(BUILD_LOG_TOOL), yes),yes)
	TARGET_LOG_TOOL :="true"
endif

ifeq ($(findstring $(TARGET_LOG_TOOL),"true"),"true")
	ifeq ($(findstring $(BUILD_LOG_NO_APP), yes),yes)
	ADDITIONAL_DEFAULT_PROPERTIES += persist.asuslog.no_app=1 \
					 persist.asuslog.fac.init=1

	PRODUCT_PACKAGES += logdumps \
				modemfaclog \
				init_factorylogtool.sh
	else
	PRODUCT_COPY_FILES += \
			$(LOCAL_PATH)/busybox:/data/debug/busybox          

	PRODUCT_PACKAGES += \
					  LogUploader \
					  packlogs.sh \
			  texfat.ko \
			  tcpdump \
					  logcommand \
					  libdumpsys_logtool\
			  DetectLogApp \
			  fdc_crashlog \
			  tool_connect \
			  logcontrol \
			  logdumps \
			  logkmsg \
			  asuslogcat \
			  logtool 
	endif
endif
